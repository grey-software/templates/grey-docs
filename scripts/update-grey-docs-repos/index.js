const shell = require('shelljs')
const fs = require('fs')
const path = require('path')

const reposPath = './repos.json'
const themePackageJsonPath = '../../packages/grey-docs/package.json'

const fileData = fs.readFileSync(path.join(__dirname, themePackageJsonPath))
const jsonData = JSON.parse(fileData)
let newVersion = jsonData.version

const greyDocsUpdateFolderPath = '~/grey-docs-update-repos'

const repos = JSON.parse(fs.readFileSync(path.join(__dirname, reposPath)))

repos.forEach((repoPath) => {
  const splitRepoArray = repoPath.split('/')
  const repoName = splitRepoArray.pop()
  console.log(repoName)
  shell.cd(greyDocsUpdateFolderPath)
  if (!fs.existsSync(path.resolve(repoName))) {
    shell.exec(`git clone ${repoPath}.git`)
  }
  shell.cd(repoName)
  shell.exec('git checkout master')
  shell.exec('git pull -f')
  const repoPackageJsonPath = path.resolve('./package.json')
  const repoPackageJsonData = JSON.parse(fs.readFileSync(repoPackageJsonPath))
  repoPackageJsonData.dependencies['grey-docs'] = `^${newVersion}`
  fs.writeFileSync(repoPackageJsonPath, JSON.stringify(repoPackageJsonData, null, 2))
  const branchName = `update-grey-docs-${new Date().toLocaleDateString()}-${new Date()
    .toTimeString()
    .substring(0, 5)
    .replace(':', '-')}`
  console.log(branchName)
  shell.exec(`git checkout -b ${branchName}`)
  shell.exec('git add --a')
  shell.exec(`git commit -m "updated grey-docs to version: ^${newVersion}"`)
  shell.exec(`git push -o merge_request.create --set-upstream origin ${branchName}`)
})
