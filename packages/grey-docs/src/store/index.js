import Vue from 'vue'
import groupBy from 'lodash.groupby'
import defu from 'defu'

export const state = () => ({
  categories: {},
  releases: [],
  settings: {
    title: 'Grey Docs',
    url: '',
    gitlabRepoId: '',
    gitlabRepoPath: '',
    pathToDocsInRepo: '',
    filled: false,
  },
})

export const getters = {
  settings(state) {
    return state.settings
  },
  githubUrls(state) {
    const { github = '' } = state.settings

    // GitHub
    return {
      repo: `https://github.com/${github}`,
      api: {
        repo: `https://api.github.com/repos/${github}`,
        releases: `https://api.github.com/repos/${github}/releases`,
      },
    }
  },
  gitlabUrls(state) {
    const { gitlabRepoId = '', pathToDocsInRepo = '' } = state.settings

    return {
      api: {
        baseUrl: `https://gitlab.com/api/v4/projects/${gitlabRepoId}`,
        blameUrl: `/repository/files/${pathToDocsInRepo}/:file_path`,
      },
    }
  },
  releases(state) {
    return state.releases
  },
  lastRelease(state) {
    return state.releases[0]
  },
}

export const mutations = {
  SET_CATEGORIES(state, categories) {
    // Vue Reactivity rules since we add a nested object
    Vue.set(state.categories, this.$i18n.locale, categories)
  },
  SET_DEFAULT_BRANCH(state, branch) {
    state.settings.defaultBranch = branch
  },
  SET_SETTINGS(state, settings) {
    state.settings = defu({ filled: true }, settings, state.settings)
    if (!state.settings.url) {
      // eslint-disable-next-line no-console
      console.warn('Please provide the `url` property in `content/setting.json`')
    }
  },
}

export const actions = {
  async fetchCategories({ commit, state }) {
    // Avoid re-fetching in production
    if (process.dev === false && state.categories[this.$i18n.locale]) {
      return
    }

    const docs = await this.$content(this.$i18n.locale, { deep: true })
      .only(['title', 'menuTitle', 'category', 'slug', 'version', 'to'])
      .sortBy('position', 'asc')
      .fetch()
    const categories = groupBy(docs, 'category')

    commit('SET_CATEGORIES', categories)
  },
  async fetchSettings({ commit }) {
    try {
      const { dir, extension, path, slug, to, createdAt, updatedAt, ...settings } = await this.$content(
        'settings'
      ).fetch()

      commit('SET_SETTINGS', settings)
    } catch (e) {
      // eslint-disable-next-line no-console
      console.warn('You can add a `settings.json` file inside the `content/` folder to customize this theme.')
    }
  },
}
