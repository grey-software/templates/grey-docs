const fs = require('fs')
const path = require('path')

const themePackageJsonPath = '../../packages/grey-docs/package.json'

const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout,
})

const filesToUpdate = {
  mainPackage: {
    path: path.join(__dirname, '../../package.json'),
  },
  examplePackage: {
    path: path.join(__dirname, '../../packages/example/package.json'),
    updateDependency: true,
  },
  templatePackage: {
    path: path.join(__dirname, '../../packages/create-grey-docs/template/package.json'),
    updateDependency: true,
  },
}

const updateVersionNumbers = (newVersion) => {
  for (const fileKey in filesToUpdate) {
    console.log(`Updating file: ${fileKey}`)
    const file = filesToUpdate[fileKey]
    const fileData = fs.readFileSync(file.path)
    const fileJson = JSON.parse(fileData)
    fileJson.version = newVersion
    if (file.updateDependency) {
      fileJson.dependencies['grey-docs'] = `^${newVersion}`
    }
    fs.writeFileSync(file.path, JSON.stringify(fileJson, null, 2))
    console.log(`Updated file: ${file.path}`)
  }
}

// Reading version number value from package.json
fs.readFile(path.join(__dirname, themePackageJsonPath), (err, data) => {
  if (err) {
    console.log(err)
    return
  }
  const jsonData = JSON.parse(data)
  let currentVersion = jsonData.version
  let majorPoint = currentVersion.indexOf('.')
  let minorPoint = currentVersion.lastIndexOf('.')
  let major = parseInt(currentVersion.substr(0, majorPoint))
  let minor = parseInt(currentVersion.substr(majorPoint + 1, minorPoint))
  let patch = parseInt(currentVersion.substr(minorPoint + 1))

  readline.question(
    'Select the version type of update.\r\n 1. Major release  (increment.0.0)\r\n 2. Minor release  (x.increment.0)\r\n 3. Patch release  (x.x.increment)\r\n >>:',
    () => {
      let choice = parseInt(readline.history[0])
      if (choice == 1) {
        major += 1
        minor = 0
        patch = 0
      } else if (choice == 2) {
        minor += 1
        patch = 0
      } else if (choice == 3) patch += 1
      else {
        console.log('The version update was unsuccessful! \rPlease pick a correct option and try again!')
        process.exit()
      }
      let newVersion = major.toString().concat('.', minor.toString(), '.', patch.toString())
      jsonData.version = newVersion

      updateVersionNumbers(newVersion)
      console.log('The version is updated successfully to : "' + newVersion + '" from " ' + currentVersion + '"')
      readline.close()
    }
  )
})
