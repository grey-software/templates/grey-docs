---
title: Components
description: 'Discover the components you can use to bring your documentation to life with the grey-docs theme!'
position: 2
category: Guide
---

### Author

<author name="Arsala" desc="President at Grey Software" linkedin-url="https://linkedin.com/in/ArsalaBangash" twitter="arsalagrey" avatar-url="https://gitlab.com/uploads/-/system/user/avatar/2274539/avatar.png" gitlab-url="https://gitlab.com/ArsalaBangash" github-url="https://github.com/ArsalaBangash" ></author>

### Profile Card

<profile-card :profile="{name: 'Arsala Grey',
avatar: 'https://gitlab.com/uploads/-/system/user/avatar/2274539/avatar.png',
      position: 'Founder & President',
      github: 'https://github.com/ArsalaBangash',
      gitlab: 'https://gitlab.com/ArsalaBangash',
      linkedin: 'https://linkedin.com/in/ArsalaBangash'}"></profile-card>

### CTA Button

<cta-button text="Landing" link="https://grey.software"></cta-button>
<cta-button text="LightBox Component" link="/components#lightbox"></cta-button>

### Sponsor Card

<SponsorCard name="ArsalaBangash" amount="$20/Month" start-date="Sponsor Since August 2020" github-url= "https://github.com/ArsalaBangash" ></SponsorCard>

<br></br>

### LightBox

<LightBox title="lightBox" url='https://images.unsplash.com/photo-1640215775139-1814b48022e4?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw0fHx8ZW58MHx8fHw%3D&auto=format&fit=crop&w=500&q=60' width="300px" height="450px"></LightBox>
