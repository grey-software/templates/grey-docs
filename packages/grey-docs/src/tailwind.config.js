/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
const path = require('path')
const plugin = require('tailwindcss/plugin')
const defaultTheme = require('tailwindcss/defaultTheme')
const selectorParser = require('postcss-selector-parser')
const { getColors } = require('theme-colors')

module.exports = ({ nuxt }) => ({
  mode: 'jit',
  important: true,
  theme: {
    minWidth: {
      0: '0',
      '1/4': '25%',
      '1/2': '50%',
      '3/4': '75%',
      full: '100%',
      144: '144px',
    },
    extend: {
      screens: {
        xs: { max: '599px' },
        sm: { min: '600px' },
        md: { min: '900px' },
        lg: { min: '1200px' },
        xl: { min: '1800px' },
      },
      fontFamily: {
        sans: ['Inter', ...defaultTheme.fontFamily.sans],
        mono: ['DM Mono', ...defaultTheme.fontFamily.mono],
        logo: ['General Sans', ...defaultTheme.fontFamily.sans],
      },
      colors: {
        primary: getColors(nuxt.options.docs.primaryColor),
        black: getColors('#181818'),
        altblack: getColors('#2d2d2d'),
        altwhite: getColors('#fefefe'),
      },
      spacing: {
        0.5: '0.125rem', // 2px
        1: '0.25rem', // 4px
        1.5: '0.375rem', // 6px
        2: '0.5rem', // 8px
        2.5: '0.625rem', // 10px
        3: '0.75rem', // 12px
        3.5: '0.875rem', // 14px
        4: '1rem', // 16px
        5: '1.25rem', // 20px
        6: '1.5rem', // 24px
        7: '1.75rem', // 28px
        8: '2rem', // 32px
        9: '2.25rem', // 36px
        10: '2.5rem', // 40px
        11: '2.75rem', // 44px
        12: '3rem', // 48px
        13: '3.25rem', // 52px
        14: '3.5rem', // 56px
        15: '3.75rem', // 60px
        16: '4rem', // 64px
        17: '4.25rem', // 68px
        18: '4.5rem', // 72px
        19: '4.75rem', // 76px
        20: '5rem', // 80px
        21: '5.25rem', // 84px
        22: '5.5rem', // 88px
        23: '5.75rem', // 92px
        24: '6rem', // 96px
        25: '6.25rem', // 100px
        26: '6.5rem', // 104px
        27: '6.75rem', // 108px
        28: '7rem', // 112px
        29: '7.25rem', // 116px
        30: '7.5rem', // 120px
        31: '7.75rem', // 124px
        32: '8rem', // 128px
        33: '8.25rem', // 132px
        34: '8.5rem', // 136px
        35: '8.75rem', // 140px
        36: '9rem', // 144px
        37: '9.25rem', // 148px
        38: '9.5rem', // 152px
        39: '9.75rem', // 156px
        40: '10rem', // 160px
        41: '10.25rem', // 164px
        42: '10.5rem', // 168px
        43: '10.75rem', // 172px
        44: '11rem', // 176px
        45: '11.25rem', // 180px
        46: '11.5rem', // 184px
        47: '11.75rem', // 188px
        48: '12rem', // 192px
        49: '12.25rem', // 196px
        50: '12.5rem', // 200px
        51: '12.75rem', // 204px
        52: '13rem', // 208px
        53: '13.25rem', // 212px
        54: '13.5rem', // 216px
        55: '13.75rem', // 220px
        56: '14rem', // 224px
        57: '14.25rem', // 228px
        58: '14.5rem', // 232px
        59: '14.75rem', // 236px
        60: '15rem', // 240px
        61: '15.25rem', // 244px
        62: '15.5rem', // 248px
        63: '15.75rem', // 252px
        64: '16rem', // 256px
        66: '16.5rem', // 264px
        68: '17rem', // 272px
        70: '17.5rem', // 280px
        72: '18rem', // 288px
        74: '18.5rem', // 296px
        76: '19rem', // 304px
        78: '19.5rem', // 312px
        80: '20rem', // 320px
        82: '20.5rem', // 328px
        84: '21rem', // 336px
        86: '21.5rem', // 344px
        88: '22rem', // 352px
        90: '22.5rem', // 360px
        92: '23rem', // 368px
        94: '23.5rem', // 376px
        96: '24rem', // 384px
        98: '24.5rem', // 392px
        100: '25rem', // 400px
        102: '25.5rem', // 408px
        104: '26rem', // 416px
        106: '26.5rem', // 424px
        108: '27rem', // 432px
        110: '27.5rem', // 440px
        112: '28rem', // 448px
        114: '28.5rem', // 456px
        116: '29rem', // 464px
        118: '29.5rem', // 472px
        120: '30rem', // 480px
        122: '30.5rem', // 488px
        124: '31rem', // 496px
        126: '31.5rem', // 504px
        128: '32rem', // 512px
      },
      typography: (theme) => ({
        DEFAULT: {
          css: {
            p: {
              fontSize: '1rem',
            },
            a: {
              textDecoration: 'none',
              fontSize: '1.04rem',
              color: theme('colors.black.400'),
              fontWeight: '600',
              transition: 'all 0.2s ease-out',
              '&:hover': {
                color: theme('colors.black.700'),
              },
            },
            h1: {
              color: '#2d2d2d',
              fontWeight: 'bold',
              fontSize: '40px',
              lineHeight: '95%',
              letterSpacing: '-0.03em',
              marginBottom: '0.25em',
            },
            h2: {
              color: '#2d2d2d',
              fontWeight: 'bold',
              fontSize: '32px',
              lineHeight: '95%',
              marginTop: '1.5rem',
              marginBottom: '1.5rem',
              borderBottomWidth: '0px',
            },
            h3: {
              color: '#2d2d2d',
              fontWeight: 'bold',
              fontSize: '24px',
              lineHeight: '95%',
              borderBottomWidth: '0px',
              marginTop: '1.5rem',
              marginBottom: '1.5rem',
            },
            h4: {
              color: '#2d2d2d',
              fontWeight: 'bold',
              fontSize: '16px',
              lineHeight: '95%',
              marginTop: '1.5rem',
              marginBottom: '1.5rem',
            },
            h5: {
              color: '#2d2d2d',
              fontWeight: 'bold',
              fontSize: '12px',
              lineHeight: '95%',
              marginTop: '1.5rem',
              marginBottom: '1.5rem',
            },
            blockquote: {
              fontWeight: '400',
              color: theme('colors.gray.600'),
              fontStyle: 'normal',
              quotes: '"\\201C""\\201D""\\2018""\\2019"',
            },
            'blockquote p:first-of-type::before': {
              content: '',
            },
            'blockquote p:last-of-type::after': {
              content: '',
            },
            code: {
              fontWeight: '400',
              backgroundColor: theme('colors.gray.100'),
              padding: theme('padding.1'),
              borderWidth: 1,
              borderColor: theme('colors.gray.200'),
              borderRadius: theme('borderRadius.default'),
            },
            'code::before': {
              content: '',
            },
            'code::after': {
              content: '',
            },
            'h3 code': {
              fontWeight: '600',
            },
            'pre code': {
              fontFamily: 'DM Mono',
            },
            'a code': {
              color: theme('colors.primary.500'),
            },
          },
        },
        dark: {
          css: {
            color: theme('colors.gray.300'),

            '[class~="lead"]': {
              color: theme('colors.gray.300'),
            },
            a: {
              color: theme('colors.gray.300'),
              '&:hover': {
                color: theme('colors.gray.100'),
              },
            },
            strong: {
              color: theme('colors.gray.100'),
            },
            'ol > li::before': {
              color: theme('colors.gray.400'),
            },
            'ul > li::before': {
              backgroundColor: theme('colors.gray.600'),
            },
            hr: {
              borderColor: theme('colors.gray.700'),
            },
            blockquote: {
              color: theme('colors.gray.400'),
              borderLeftColor: theme('colors.gray.700'),
            },
            h1: {
              color: theme('colors.gray.100'),
            },
            h2: {
              color: theme('colors.gray.100'),
              borderBottomColor: theme('colors.gray.800'),
            },
            h3: {
              color: theme('colors.gray.100'),
              borderBottomColor: theme('colors.gray.800'),
            },
            h4: {
              color: theme('colors.gray.100'),
            },
            'figure figcaption': {
              color: theme('colors.gray.400'),
            },
            code: {
              color: theme('colors.gray.100'),
              backgroundColor: theme('colors.gray.800'),
              borderWidth: 0,
            },
            'a code': {
              color: theme('colors.primary.500'),
            },
            thead: {
              color: theme('colors.gray.100'),
              borderBottomColor: theme('colors.gray.600'),
            },
            'tbody tr': {
              borderBottomColor: theme('colors.gray.700'),
            },
          },
        },
      }),
      maxHeight: {
        '(screen-16)': 'calc(100vh - 4rem)',
      },
      inset: {
        16: '4rem',
      },
      transitionProperty: {
        padding: 'padding',
      },
    },
  },
  variants: {
    margin: ['responsive', 'last'],
    padding: ['responsive', 'hover'],
    backgroundColor: ['responsive', 'hover', 'focus', 'dark', 'dark-focus, dark-hover'],
    textColor: ['responsive', 'hover', 'focus', 'dark', 'dark-hover', 'dark-focus'],
    borderColor: ['responsive', 'hover', 'focus', 'dark', 'dark-focus'],
    borderWidth: ['responsive', 'first', 'last'],
    typography: ['responsive', 'dark'],
  },
  plugins: [
    plugin(function ({ addVariant, prefix, e }) {
      addVariant('dark', ({ modifySelectors, separator }) => {
        modifySelectors(({ selector }) => {
          return selectorParser((selectors) => {
            selectors.walkClasses((sel) => {
              sel.value = `dark${separator}${sel.value}`
              sel.parent.insertBefore(sel, selectorParser().astSync(prefix('.dark-mode ')))
            })
          }).processSync(selector)
        })
      })

      addVariant('dark-hover', ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          return `.dark-mode .${e(`dark-hover${separator}${className}`)}:hover`
        })
      })

      addVariant('dark-focus', ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          return `.dark-mode .${e(`dark-focus${separator}${className}`)}:focus`
        })
      })
    }),
    require('@tailwindcss/typography')({
      className: 'typography',
    }),
    require('tailwind-css-variables')({
      colors: 'color',
      screens: false,
      fontFamily: false,
      fontSize: false,
      fontWeight: false,
      lineHeight: false,
      letterSpacing: false,
      backgroundSize: false,
      borderWidth: false,
      borderRadius: false,
      width: false,
      height: false,
      minWidth: false,
      minHeight: false,
      maxWidth: false,
      maxHeight: false,
      padding: false,
      margin: false,
      boxShadow: false,
      zIndex: false,
      opacity: false,
    }),
  ],
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: process.env.NODE_ENV === 'production',
    content: [
      'content/**/*.md',
      path.join(nuxt.options.rootDir, 'components/**/*.vue'),
      path.join(__dirname, 'components/**/*.vue'),
      path.join(__dirname, 'layouts/**/*.vue'),
      path.join(__dirname, 'pages/**/*.vue'),
      path.join(__dirname, 'plugins/**/*.js'),
      'nuxt.config.js',
    ],
    options: {
      safelist: ['dark-mode'],
    },
  },
  corePlugins: {
    container: false,
  },
})
