# create-grey-docs

Create a documentation website with Grey Software's grey-docs theme!

## Usage

With [yarn](https://yarnpkg.com/en/):

```bash
yarn create grey-docs <project-name>
```

Or with [npx](https://www.npmjs.com/package/npx) (`npx` is shipped by default since [npm](https://www.npmjs.com/get-npm)
`5.2.0`)

```bash
npx create-ngrey-docs <project-name>
```

Or starting with npm v6.1 you can do:

```bash
npm init grey-docs <project-name>
```
